//enablePlugins(ScalaJSPlugin)

name := "TST Take Home"
scalaVersion := "2.12.8"

// This is an application with a main method
scalaJSUseMainModuleInitializer := true

// uTest settings
libraryDependencies += "com.lihaoyi" %%% "utest" % "0.6.6" % "test"
testFrameworks += new TestFramework("utest.runner.Framework")
