# Instructions

- Install [sbt](https://www.scala-sbt.org)
- Run `sbt test` to execute test suite included in project
- Tests are written using [utest](https://github.com/lihaoyi/utest)

## Using Scala-JS

- Uncomment first line of `built.sbt` to use [scala-js](https://www.scala-js.org)
- So install [node](https://nodejs.org)