package tst.webapp

import scala.annotation.tailrec

object GroupRates {

  type RateCode = String

  type RateGroup = String

  type CabinCode = String

  type Price = BigDecimal

  case class Rate(rateCode: RateCode, rateGroup: RateGroup)

  case class CabinPrice(cabinCode: RateCode, rateCode: RateCode, price: Price)

  case class BestGroupPrice(
      cabinCode: RateCode,
      rateCode: RateCode,
      price: Price,
      rateGroup: RateGroup
  )

  def toGroupPrices(rates: Seq[Rate], prices: Seq[CabinPrice]) = {
    val codeToRate = rates.groupBy(_.rateCode)
    for {
      p <- prices;
      r <- codeToRate.get(p.rateCode) if !r.isEmpty
    } yield BestGroupPrice(p.cabinCode, p.rateCode, p.price, r.head.rateGroup)
  }

  @tailrec
  def minByPrice(
      xs: Seq[BestGroupPrice],
      acc: Map[(CabinCode, RateGroup), BestGroupPrice]
  ): Seq[BestGroupPrice] =
    xs match {
      case h :: t => {
        val k = (h.cabinCode, h.rateGroup)
        val curMin = acc.getOrElse(k, h)
        if (h.price <= curMin.price) {
          minByPrice(t, acc + (k -> h))
        } else {
          minByPrice(t, acc)
        }
      }
      case _ => {
        acc.values.toSeq
      }
    }

  def bestPrice(
      rates: Seq[Rate],
      prices: Seq[CabinPrice]
  ): Seq[BestGroupPrice] = {
    val groupPrices = toGroupPrices(rates, prices)
    val bestPrices = minByPrice(groupPrices, Map())
    bestPrices
  }
}
