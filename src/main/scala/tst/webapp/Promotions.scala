package tst.webapp

object Promotions {

  type Code = String

  case class Promotion(code: Code, notCombinedWith: Seq[Code])

  case class PromotionCombo(promotionCodes: Seq[Code])

  private def combos(promos: Seq[Promotion]): List[List[Code]] =
    promos match {
      case Seq() => List(Nil)
      case Seq(Promotion(c, e), t @ _*) => {
        val eSet = e.toSet
        for {
          prevCombo <- combos(t)
          combo = prevCombo.filterNot(eSet)
          prevChecked = if (prevCombo == combo) {
            Nil
          } else {
            prevCombo
          }
          y <- List(c :: combo, prevChecked) if y != Nil
        } yield y
      }
    }

  def allCombinablePromotions(
      allPromotions: Seq[Promotion]
  ): Seq[PromotionCombo] =
    combos(allPromotions)
      .sortBy(_.head)
      .map(PromotionCombo)

  def combinablePromotions(
      promotionCode: Code,
      allPromotions: Seq[Promotion]
  ): Seq[PromotionCombo] =
    combos(allPromotions)
      .filter(_.contains(promotionCode))
      .sortBy(_.head)
      .map(_.sortBy(_ != promotionCode))
      .map(PromotionCombo)
}
