package tst.webapp

import utest._

import tst.webapp.Promotions._

object PromotionsTest extends TestSuite {

  val promotions = Seq(
    Promotion("P1", Seq("P3")),
    Promotion("P2", Seq("P4", "P5")),
    Promotion("P3", Seq("P1")),
    Promotion("P4", Seq("P2")),
    Promotion("P5", Seq("P2"))
  )

  val expectedAllPromosCombos = Seq(
    PromotionCombo(Seq("P1", "P2")),
    PromotionCombo(Seq("P1", "P4", "P5")),
    PromotionCombo(Seq("P2", "P3")),
    PromotionCombo(Seq("P3", "P4", "P5"))
  )

  val expectedPromosForP1 = Seq(
    PromotionCombo(Seq("P1", "P2")),
    PromotionCombo(Seq("P1", "P4", "P5"))
  )

  val expectedPromosForP3 = Seq(
    PromotionCombo(Seq("P3", "P2")),
    PromotionCombo(Seq("P3", "P4", "P5"))
  )

  def tests = Tests {
    'AllPromoCombos - {
      assert(
        expectedAllPromosCombos ==
          allCombinablePromotions(promotions)
      )
    }

    'CombinablePromos - {
      assert(
        expectedPromosForP1 ==
          combinablePromotions("P1", promotions)
      )
      assert(
        expectedPromosForP3 ==
          combinablePromotions("P3", promotions)
      )
    }
  }
}
